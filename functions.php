<?php

if ( ! function_exists('edu_enqueues') ) {
	function edu_enqueues() {
		
		$version = rand(0,100).'.12.2';
		
		// Styles
		wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/js/bootstrap/css/bootstrap.min.css', false, $version, null);
		
		wp_enqueue_style('edu-css', get_template_directory_uri() . '/css/edu.css', false, $version);

		// Scripts
		wp_enqueue_script('font-awesome-config-js', get_template_directory_uri() . '/js/font-awesome-config.js', false, $version, null);
		wp_enqueue_script('font-awesome', get_template_directory_uri() . '/js/font-awesome/js/fontawesome-all.min.js', false, $version, null);
		wp_enqueue_script('modernizr', get_template_directory_uri() . '/js/modernizr.min.js', false, $version, true);
		wp_enqueue_script('jquery-3.3.1', get_template_directory_uri() . '/js/jquery.min.js', false, $version, true);
		wp_enqueue_script('popper', get_template_directory_uri() . '/js/popper.min.js', false, $version, true);
		wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap/js/bootstrap.min.js', false, $version, true);
		
		wp_enqueue_script('edu-js', get_template_directory_uri() . '/js/edu.js', false, $version, true);

		if (is_singular() && comments_open() && get_option('thread_comments')) {
			wp_enqueue_script('comment-reply');
		}
	}
}
add_action('wp_enqueue_scripts', 'edu_enqueues', 100);

// Less stuff in <head>
if ( ! function_exists('edu_cleanup_head') ) {
  function edu_cleanup_head() {
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'index_rel_link');
    remove_action('wp_head', 'feed_links', 2);
    remove_action('wp_head', 'feed_links_extra', 3);
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
    remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
    remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('wp_print_styles', 'print_emoji_styles');
  }
}
add_action('init', 'edu_cleanup_head');


// Show less info to users on failed login for security.
if ( ! function_exists('show_less_login_info') ) {
  function show_less_login_info() {
      return "<strong>ERROR</strong>: Stop guessing!";
  }
}
add_filter( 'login_errors', 'show_less_login_info' );


// Do not generate and display WordPress version
if ( ! function_exists('edu_remove_generator') ) {
  function edu_remove_generator()  {
    return '';
  }
}
add_filter( 'the_generator', 'edu_remove_generator' );

// Remove Query Strings From Static Resources
if ( ! function_exists('edu_remove_script_version') ) {
  function edu_remove_script_version( $src ) {
    $parts = explode( '?', $src );
    return $parts[0];
  }
}
add_filter( 'script_loader_src', 'edu_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', 'edu_remove_script_version', 15, 1 );

// WP related initialization
if ( ! function_exists('edu_setup') ) {
	function edu_setup() {
		add_editor_style('theme/css/editor-style.css');

		add_theme_support('title-tag');

		add_theme_support('post-thumbnails');

		update_option('thumbnail_size_w', 285); /* internal max-width of col-3 */
		update_option('small_size_w', 350); 	/* internal max-width of col-4 */
		update_option('medium_size_w', 730); 	/* internal max-width of col-8 */
		update_option('large_size_w', 1110); 	/* internal max-width of col-12 */

		if ( ! isset($content_width) ) {
			$content_width = 1100;
		}
		add_theme_support('automatic-feed-links');
		
		register_nav_menu('footmenu', __('Footer Menu', 'edu'));
		register_nav_menu('topmenu', __('Top Menu', 'edu'));
	}
}
add_action('init', 'edu_setup');

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> __('Theme Options', 'edu'),
		'menu_title'	=> __('Theme Options', 'edu'),
		'menu_slug' 	=> 'edu-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	/*
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'edu-general-settings',
	));
	*/
}

// Make WP date Bootstrap Compatible
if ( ! function_exists( 'edu_post_date' ) ) {
	function edu_post_date() {
		if ( in_array( get_post_type(), array( 'post', 'attachment' ) ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';

			if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
				$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time> <time class="updated" datetime="%3$s">(updated %4$s)</time>';
			}

			$time_string = sprintf( $time_string,
				esc_attr( get_the_date( 'c' ) ),
				get_the_date(),
				esc_attr( get_the_modified_date( 'c' ) ),
				get_the_modified_date()
			);

			echo $time_string;
		}
	}
}

// Bootstrap Compatible Excerpt More button
if ( ! function_exists('edu_excerpt_more') ) {
	function edu_excerpt_more() {
		return '&hellip;</p><p><a class="btn btn-primary" href="'. get_permalink() . '">' . __('Continue reading', 'edu') . ' <i class="fas fa-arrow-right"></i>' . '</a></p>';
	}
}
add_filter('excerpt_more', 'edu_excerpt_more');


// Bootstrap Compatible Navbar
class edu_walker_nav_menu extends Walker_Nav_menu {

	function start_lvl( &$output, $depth = 0, $args = array() ){ // ul
		$indent = str_repeat("\t",$depth); // indents the outputted HTML
		$submenu = ($depth > 0) ? ' sub-menu' : '';
		$output .= "\n$indent<ul class=\"dropdown-menu$submenu depth_$depth\">\n";
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){ // li a span

		$indent = ( $depth ) ? str_repeat("\t",$depth) : '';

		$li_attributes = '';
		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		$classes[] = ($args->walker->has_children) ? 'dropdown' : '';
		$classes[] = ($item->current || $item->current_item_ancestor) ? 'active' : '';
		$classes[] = 'nav-item';
		$classes[] = 'nav-item-' . $item->ID;
		if( $depth && $args->walker->has_children ){
			$classes[] = 'dropdown-menu dropdown-menu-right';
		}

		$class_names =  join(' ', apply_filters('nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = ' class="' . esc_attr($class_names) . '"';

		$id = apply_filters('nav_menu_item_id', 'menu-item-'.$item->ID, $item, $args);
		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '<li ' . $id . $value . $class_names . $li_attributes . '>';

		$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr($item->target) . '"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr($item->url) . '"' : '';

		$attributes .= ( $args->walker->has_children ) ? ' class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"' : ' class="nav-link"';

		$item_output = $args->before;
		$item_output .= ( $depth > 0 ) ? '<a class="dropdown-item"' . $attributes . '>' : '<a' . $attributes . '>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters ( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

	}
}

// Initialize widget areas
function edu_widgets_init() {

	// Sidebar
	register_sidebar( array(
		'name'            => __( 'Sidebar', 'edu' ),
		'id'              => 'sidebar-widget-area',
		'description'     => __( 'The sidebar widget area', 'edu' ),
		'before_widget'   => '<section class="%1$s %2$s">',
		'after_widget'    => '</section>',
		'before_title'    => '<h2 class="h4">',
		'after_title'     => '</h2>',
	) );

	/*
	Footer (1, 2, 3, or 4 areas)

	Flexbox `col-sm` gives the correct the column width:

	* If only 1 widget, then this will have full width ...
	* If 2 widgets, then these will each have half width ...
	* If 3 widgets, then these will each have third width ...
	* If 4 widgets, then these will each have quarter width ...
	... above the Bootstrap `sm` breakpoint.
	*/

	register_sidebar( array(
		'name'            => __( 'Footer', 'edu' ),
		'id'              => 'footer-widget-area',
		'description'     => __( 'The footer widget area', 'edu' ),
		'before_widget'   => '<div class="%1$s %2$s col-sm">',
		'after_widget'    => '</div>',
		'before_title'    => '<h2 class="h4">',
		'after_title'     => '</h2>',
	) );

}
add_action( 'widgets_init', 'edu_widgets_init' );

// Bootstrap Compatible Search Form
if ( ! function_exists('edu_search_form') ) {
  function edu_search_form( $form ) {
    $form = '<form class="form-inline mb-3" role="search" method="get" id="searchform" action="' . home_url('/') . '" >
      <input class="form-control mr-sm-1" type="text" value="' . get_search_query() . '" placeholder="' . esc_attr_x('Search', 'edu') . '..." name="s" id="s" />
      <button type="submit" id="searchsubmit" value="'. esc_attr_x('Search', 'edu') .'" class="btn btn-primary"><i class="fas fa-search"></i></button>
    </form>';
    return $form;
  }
}
add_filter( 'get_search_form', 'edu_search_form' );


// Bootstrap compatible pagination
if ( ! function_exists( 'edu_pagination' ) ) {
	function edu_pagination() {
		global $wp_query;
		$big = 999999999; // This needs to be an unlikely integer
		// For more options and info view the docs for paginate_links()
		// http://codex.wordpress.org/Function_Reference/paginate_links
		$paginate_links = paginate_links( array(
			'base' => str_replace( $big, '%#%', get_pagenum_link($big) ),
			'current' => max( 1, get_query_var('paged') ),
			'total' => $wp_query->max_num_pages,
			'mid_size' => 5,
			'prev_next' => True,
			'prev_text' => __('<i class="fas fa-angle-left"></i> Newer', 'edu'),
			'next_text' => __('Older <i class="fas fa-angle-right"></i>', 'edu'),
			'type' => 'list'
		) );
		$paginate_links = str_replace( "<ul class='page-numbers'>", "<ul class='pagination'>", $paginate_links );
		$paginate_links = str_replace( "<li>", "<li class='page-item'>", $paginate_links );
		$paginate_links = str_replace( "<li class='page-item'><span aria-current='page' class='page-numbers current'>", "<li class='page-item active'><a class='page-link' href='#'>", $paginate_links );
		$paginate_links = str_replace( "<a", "<a class='page-link' ", $paginate_links );

		$paginate_links = str_replace( "</span>", "</a>", $paginate_links );
		$paginate_links = preg_replace( "/\s*page-numbers/", "", $paginate_links );
		// Display the pagination if more than one page is found
		if ( $paginate_links ) {
			echo $paginate_links;
		}
	}
}

if ( ! function_exists( 'edu_split_post_pagination' ) ) {
	// Single Post or Page Pagination
	function edu_split_post_pagination($wp_links){
		global $post;

		$post_base = trailingslashit( get_site_url(null, $post->post_name) );
		$wp_links = trim(str_replace(array('<p>Pages: ', '</p>'), '', $wp_links));

		if ( empty($wp_links) ) {
			return '';
		}

		// Split links at spaces
		$splits = explode(' ', $wp_links );
		$links = array();
		$current_page = 1;

		// Loop over split array, correct and rejoin split links
		foreach( $splits as $key => $split ){
			if( is_numeric($split) ) {
				$links[] = $split;
				$current_page = $split;
			} else if ( strpos($split, 'href') === false ) {
				$links[] = $split . ' ' . $splits[$key + 1];
			}
		}

		$num_pages = count($links);

		// Start UL
		$output .= '<ul class="pagination justify-content-center mt-5">';

		// Page status
		$output .= '<li class="page-item disabled"><a class="page-link">Page ' . $current_page . ' of ' . $num_pages . '</a></li>';

		// Skip to first
		if ( $current_page == 1 ) {
			$output .= '<li class="page-item disabled"><a class="page-link">';
		} else {
			$output .= '<li class="page-item"><a class="page-link" href="' . $post_base . '">';
		}
		$output .= '<i class="fas fa-angle-double-left"></i></a></li>';

		// Prev
		if ( $current_page == 1 ) {
			$output .= '<li class="page-item disabled"><a class="page-link">';
		} else {
			$output .= '<li class="page-item"><a class="page-link" href="' . $post_base . ($current_page - 1) . '">';
		}
		$output .= '<i class="fas fa-angle-left"></i></a></li>';

		// Pagination
		foreach( $links as $key => $link ) {
			$temp_key = $key + 1;
			if ( $current_page == $temp_key ) {
				$output .= '<li class="page-item active"><a class="page-link" href="' . $post_base . $temp_key . '">' . $temp_key . '</a></li>';
			} else {
				$output .= '<li class="page-item"><a class="page-link" href="' . $post_base . $temp_key . '">' . $temp_key . '</a></li>';
			}
		}

		// Next
		if ( $current_page == $num_pages ) {
			$output .= '<li class="page-item disabled"><a class="page-link">';
		} else {
			$output .= '<li class="page-item"><a class="page-link" href="' . $post_base . ($current_page + 1) . '">';
		}
		$output .= '<i class="fas fa-angle-right"></i></a></li>';

		// Skip to last
		if ( $current_page == $num_pages ) {
			$output .= '<li class="page-item disabled"><a class="page-link">';
		} else {
			$output .= '<li class="page-item"><a class="page-link" href="' . $post_base . $num_pages . '">';
		}
		$output .= "<i class=\"fas fa-angle-double-right\"></i></a></li>";

		// Close UL
		$output .= '</ul>';

		return $output;
	}
}
add_filter('wp_link_pages', 'edu_split_post_pagination');
