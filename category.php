<?php get_header(); ?>

<div class="container-responsive mt-5">
  <div class="row">
	
	<?php get_sidebar(); ?>
	
    <div class="col-sm">
      <div id="content" role="main">
        <header class="mb-4 border-bottom">
          <h1>
            <?php _e('Category: ', 'edu'); echo single_cat_title(); ?>
          </h1>
        </header>
        <?php get_template_part('loops/index-loop'); ?>
      </div><!-- /#content -->
    </div>

  </div><!-- /.row -->
</div><!-- /.container-responsive -->

<?php get_footer(); ?>
