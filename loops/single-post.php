<?php
/**!
 * The Single Posts Loop
 */
?>

<?php if(have_posts()): while(have_posts()): the_post(); ?>
  <article role="article" id="post_<?php the_ID()?>" <?php post_class()?>>
    <header class="mb-4">
      <h1>
        <?php the_title()?>
      </h1>
      <div class="header-meta text-muted">
        <?php
          _e('By ', 'edu');
          the_author_posts_link();
          _e(' on ', 'edu');
          edu_post_date();
        ?>
      </div>
    </header>
    <main>
      <?php
        the_post_thumbnail();
        the_content();
        wp_link_pages();
      ?>
    </main>
    <footer class="mt-5 border-top pt-3">
      <p>
        <?php _e('Category: ', 'edu'); the_category(', ') ?> | <?php if (has_tag()) { the_tags('Tags: ', ', '); ?> | <?php } _e('Comments', 'edu'); ?>: <?php comments_popup_link(__('None', 'edu'), '1', '%'); ?>
      </p>
    </footer>
  </article>
<?php
    if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;
  endwhile; else:
    wp_redirect(esc_url( home_url() ) . '/404', 404);
    exit;
  endif;
?>
<div class="row mt-5 border-top pt-3">
  <div class="col">
    <?php previous_post_link('%link', '<i class="fas fa-fw fa-arrow-left"></i> Previous post: '.'%title'); ?>
  </div>
  <div class="col text-right">
    <?php next_post_link('%link', 'Next post: '.'%title' . ' <i class="fas fa-fw fa-arrow-right"></i>'); ?>
  </div>
</div>
